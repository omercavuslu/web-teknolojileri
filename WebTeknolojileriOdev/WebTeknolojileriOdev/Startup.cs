﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebTeknolojileriOdev.Startup))]
namespace WebTeknolojileriOdev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
